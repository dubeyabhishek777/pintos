/* Fixed-point real arithmetic */
#ifndef FIXED_POINT_ARITH
#define FIXED_POINT_ARITH

/* Fixed point convention used P=17, Q=14 */

#define Q 14
#define FRACTION 1 << Q

/* Here x and y are fixed-point number, n is an integer,
   P.Q fixed format where P + Q = 31 bits and FRACTION = 1 << Q */

#define CONVERT_TO_FP(n) (n) * (FRACTION)
#define CONVERT_TO_INT_ZERO(x) (x) / (FRACTION)
#define CONVERT_TO_NEAREST_INT(x) ((x) >= 0 ? ((x) + (FRACTION) / 2) / (FRACTION)\
                                            : ((x) - (FRACTION) / 2) / (FRACTION))
#define ADD(x, y) (x) + (y)
#define SUB(x, y) (x) - (y)
#define MULT(x, y) ((int64_t)(x)) * (y) / (FRACTION)
#define DIV(x, y) ((int64_t)(x)) * (FRACTION) / (y)

#define ADD_INT(x, n) (x) + (n) * (FRACTION)
#define SUB_INT(x, n) (x) - (n) * (FRACTION)
#define MULT_INT(x, n) (x) * (n)
#define DIV_INT(x, n) (x) / (n)

#endif
